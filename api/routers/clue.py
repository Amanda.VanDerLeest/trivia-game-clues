from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
from categories import CategoryOut

router = APIRouter()

class Clue(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: list[CategoryOut]
    canon: bool

@router.get("/api/clue/{clue_id}"
response_m)
def get_clue(clue_id:int):
    with psycopg.connect()